import { createApp } from 'vue'
import App from './App.vue'
import { createStore } from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import PrimeVue from 'primevue/config';

import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

const store = createStore({
    state () {
        return {
            count: 0,
            words:[]
        }
    },
    actions: {
        loadWords (state) {
            state.words = [];
            return axios.get('https://api.json-generator.com/templates/WPdZYfCW_VZG/data?access_token=9a3xhlnyq59ux0m6a744clqc5u5me6lwz2abmrp5')
                .then(function (response) {
                    state.words = response
                    return Promise.resolve(state.words);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        saveToLocalStorageSignal(state, context) {
            return localStorage.setItem("wordsData", JSON.stringify(context));
        },
        loadFromLocalStorageSignal() {
            let data = localStorage.getItem("wordsData");
            if(data !== null) {
                data = JSON.parse(data);
            }
            return data;
        },
    }
})

createApp(App).use(store).use(VueAxios, axios).use(PrimeVue).mount('#app');
